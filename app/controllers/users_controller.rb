class UsersController < ApplicationController
    before_action :set_user, only: [:update, :show, :delete]
    respond_to :json

    def show
        respond_with @user
    end

    def create
        @user = User.new(user_params)

        if @user.save
            respond_with @user, status: :created
        else
            render json: @user.errors, status: :unprocessable_entity
        end
    end

    def update
        if @user.update(user_params)
            respond_with @user, status: :ok
        else
            render json: @user.errors, status: :unprocessable_entity
        end
    end

    def delete
        @user.destroy
    end

    private
    def set_user
        @user = User.find(params[:id])
    end

    def user_params
        params.require(:user).permit(:email, :password, :password_confirmation, :name)
    end
end
