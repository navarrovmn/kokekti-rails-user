module Render
    extend ActiveSupport::Concern

    def render_error(title, detail, status)
        error = {
            "errors": [
                {
                    "title": title,
                    "detail": detail
                }
            ]
        }

        render json: error, status: status
    end
end
