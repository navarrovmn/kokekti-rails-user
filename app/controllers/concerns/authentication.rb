module Authentication
    extend ActiveSupport::Concern

    def login
        @user = User.find_by(email: params[:email])
        if @user
            if @user.authenticate(params[:password])
                @token = generate_token
                respond_with @user
            else
                render_error "Authentication", "Wrong password", :unauthorized
            end
        else
            render_error "Authentication", "No email was given to authenticate", :bad_request
        end
    end

    def jwt_authenticated?
        if request.headers['Authentication'].present?
            decoded_token = decode(request.headers['Authentication'])
            if decoded_token.size == 2
                @request_user = User.find(decoded_token.first["user_id"])
            else
                render_error *decoded_token.first
            end
        else
            render_error "Authentication", "No token was given", :unauthorized
        end
    end

    private
    def encode(payload = {}, exp_time = 60)
        payload.merge!(exp: Time.now.to_i + exp_time * 60)
        JWT.encode(payload, Rails.application.credentials[Rails.env.to_sym][:hmac_secret] || ENV['TEST_KEY'], 'HS256')
    end

    def decode(token)
        begin
            JWT.decode(token, Rails.application.credentials[Rails.env.to_sym][:hmac_secret] || ENV['TEST_KEY'], true, { algorithm: 'HS256' })
        rescue JWT::ExpiredSignature
            [["Authentication", "Token has expired", :unauthorized]]
        rescue
            [["Authentication", "Invalid token", :unauthorized]]
        end
    end

    def generate_token
        auth_token = encode({ user_id: @user.id }, 60)
    end
end