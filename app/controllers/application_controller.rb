class ApplicationController < ActionController::API
    include ActionController::MimeResponds
    include Render
    include Authentication

    respond_to :json

    before_action :jwt_authenticated?, except: [:login, :welcome]

    def welcome
        render json: { "Konekti-Rails-User": "v1.0" }
    end
end
